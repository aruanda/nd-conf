#!/bin/bash

if [ ! -d /automobi/app/node_modules ]
then
  OLD_PATH=$PWD

  cd /nd-conf/app
  npm install

  cd $OLD_PATH
fi

echo ""
echo "------------------------------"
echo "Ambiente pronto para trabalhar"
echo "------------------------------"
echo ""

exec "$@"
