.PHONY: run in stop clean build build-env

run:
	docker-compose run --rm -p 8080:8080 dev

in:
	docker exec -i -t $(shell docker-compose ps | grep run | cut -d" " -f 1) /bin/bash

stop:
	docker-compose stop

clean:
	docker-compose down
	docker volume ls -qf dangling=true | xargs -r docker volume rm

build-env:
	sh ./docker/dockerfile-ids.sh
	docker build -t nd-conf -f ./docker/Dockerfile ./docker

build-env-base:
	docker build -t malera/es6:base -f ./docker/hub/DockerfileES6 ./docker/hub
	docker push malera/es6:base

build-env-development:
	docker build -t malera/es6:dev -f ./docker/hub/DockerfileDev ./docker/hub
	docker push malera/es6:dev
