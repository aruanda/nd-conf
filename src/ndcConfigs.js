export default async ({modules}) => {
  let allConfigs = {}
  modules.forEach(({configs}) => Object.assign(allConfigs, configs))
  return allConfigs
}
