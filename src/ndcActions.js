import execute from 'nd-conf/ndcExecute'

export default async ({modules}) => {
  let allActions = {}

  modules.forEach(({actions}) => {
    if (!actions) return
    Object.assign(allActions, actions)
  })

  return Object.assign(allActions, {execute})
}
