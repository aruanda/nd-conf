const schemasToObject = (schemas) => {
  let objSchemas = {}
  if (!schemas) return objSchemas

  Object.keys(schemas).forEach((schemaName) => {
    const schema = schemas[schemaName]
    Object.assign(objSchemas, {[schemaName]: schema})
  })

  return objSchemas
}

export default async ({modules}) => {
  let allSchemas = {}
  modules.forEach(({schemas}) => Object.assign(allSchemas, schemasToObject(schemas)))
  return allSchemas
}
