export default ({modules}) => {
  let allRoutes = {}

  modules.forEach(({routes}) => {
    if (!routes) return

    routes.forEach((route) => {
      const {verb, path} = route
      const uri = `${verb.toUpperCase()}: ${path}`
      allRoutes[uri] = route
    })
  })

  return allRoutes
}
