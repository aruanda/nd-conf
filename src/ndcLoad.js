import ndcRoutes from 'nd-conf/ndcRoutes'
import ndcActions from 'nd-conf/ndcActions'
import ndcConfigs from 'nd-conf/ndcConfigs'
import ndcSchemas from 'nd-conf/ndcSchemas'

export default async (server) => {
  const routes = await ndcRoutes(server)
  const actions = await ndcActions(server)
  const configs = await ndcConfigs(server)
  const schemas = await ndcSchemas(server)
  return { routes, actions, configs, schemas }
}
