import ndcLoad from 'nd-conf/ndcLoad'

export default async (server) => {
  const engines = await ndcLoad(server)
  let context = Object.assign({}, server, engines)
  await Promise.all(Object.values(engines.configs).map(async config => await config(context)))
  return context
}
