Node Configuration
===============================
Based to frameworks by Action

---------------------------------------------

## Pré-requisitos
  * [GIT](https://git-scm.com/)
  * [Docker](https://get.docker.com)
  * [Docker Compose](https://docs.docker.com/compose/)

---------------------------------------------

## Clonar projeto
```sh
$ git clone git@bitbucket.org:aruanda/nd-conf.git nd-conf
$ cd nd-conf/
$ make build
$ make run

-----------------------------
Máquina pronta para trabalhar
-----------------------------

nd-conf:~/app(master)$ npm install
nd-conf:~/app(master)$ npm start
```
---------------------------------------------

## Comandos para desenvolvimento
Todos os comandos devem ser executados na pasta onde se encontra o Makefile
ou raiz do projeto.


##### Para criar no ambiente de desenvolvimento (baixa e cria imagem de desenvolvimento)
```sh
$ make build-env
```

##### Para entrar no ambiente de desenvolvimento (entra no container)
```sh
$ make run
```

##### Para pausar ambiente de desenvolvimento (executar fora do container)
```sh
$ make stop
```

##### Para abrir um novo terminal durante desenvolvimento
```sh
$ make in
```

##### Para limpar ambiente de desenvolvimento (bom executar uma vez por semana ou sempre)
```sh
$ make clean
```
